import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Speech } from '../shared/models/speech';
import { SpeechService } from '../shared/services/speech.service'
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { SpeechDataValidator } from '../shared/validators/speech.validator';
import { MessageConstants } from '../shared/constants/message.constants';
import { EmitterService } from '../shared/services/emitter.service';

@Component({
  selector: 'app-root',
  templateUrl: './newspeech.component.html',
  styleUrls: ['./newspeech.component.css'],
  providers: [SpeechDataValidator]
})
export class NewSpeechComponent {
  speech: Speech = new Speech();
  myDate: any = { date: { year: 2016, month: 10, day: 9 } };
  myDatePickerOptions: IMyDpOptions = {
    todayBtnTxt: 'Today',
    dateFormat: 'yyyy-mm-dd',
  };
  error = {
    isError: false,
    errorMessage: ""
  };

  constructor(private validator: SpeechDataValidator, private router: Router) { }

  ngOnInit() {
    EmitterService.get("loader").emit(false);
  }

  submitSpeech() {
    EmitterService.get("loader").emit(true);
    if(this.validator.validateData(this.speech)){
      this.error.isError = false;
      setTimeout(() => {
        EmitterService.get("loader").emit(false);
        this.router.navigate(['speeches']);
        EmitterService.notify("notification").emit('New Speech has been saved.');
      }, 1500)
    } else {
      this.error.isError = true;
      this.error.errorMessage = MessageConstants.REQUIRED_FIELDS;
      EmitterService.get("loader").emit(false);
    }
  }
}
