import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MyDatePickerModule } from 'mydatepicker';
import { FormsModule } from '@angular/forms';
import { SpinnerComponentModule } from 'ng2-component-spinner';
import { ToasterModule } from 'angular2-toaster';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Components
import { AppComponent } from './app.component';
import { SpeechComponent } from '../speech/speech.component';
import { SpeechOverviewComponent } from '../speech_overview/overview.component';
import { NewSpeechComponent } from '../speech_submit/newspeech.component';

// Services
import { SpeechService } from '../shared/services/speech.service';
import { HttpClient } from '../shared/services/httpClient.service';

const appRoutes: Routes = [
  { path: 'speech/:id', component: SpeechOverviewComponent },
  { path: 'speech-new', component: NewSpeechComponent },
  { path: 'speeches-search', component: SpeechComponent },
  { path: '**', component: SpeechComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    SpeechComponent,
    SpeechOverviewComponent,
    NewSpeechComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
      // { enableTracing: true }
    ),
    HttpModule,
    MyDatePickerModule,
    SpinnerComponentModule,
    ToasterModule,
    BrowserAnimationsModule
  ],
  providers: [
    DatePipe,
    HttpClient,
    SpeechService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
