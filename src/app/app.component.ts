import { Component } from '@angular/core';
import { EmitterService } from '../shared/services/emitter.service';
import { ToasterService } from 'angular2-toaster';
import { ToasterConfig } from 'angular2-toaster';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [EmitterService]
})
export class AppComponent {
  public loader: boolean = false;
  private subscription;
  toasterconfig : ToasterConfig = new ToasterConfig();

  constructor(private toasterService: ToasterService) {
    EmitterService.get("loader").subscribe(data => {
      this.loader = data;
    });
    EmitterService.notify("notification").subscribe(data => {
      this.toasterService.pop('success', 'Notification', data);
    });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}
