import { Component } from '@angular/core';
import { Speech } from '../shared/models/speech';
import { SpeechService } from '../shared/services/speech.service';
import { EmitterService } from '../shared/services/emitter.service';

@Component({
  selector: 'app-root',
  templateUrl: './speech.component.html',
  styleUrls: ['./speech.component.css']
})
export class SpeechComponent {
  speeches: Speech[];
  search: string;

  constructor(private speechService: SpeechService) { }

  ngOnInit() {
    this.getSpeeches();
  }

  searchSpeeches() {
    this.getSpeeches(this.search);
  }

  private getSpeeches(search: string = null) {
    EmitterService.get("loader").emit(true);
    setTimeout(() => {
      this.speechService.getSpeeches(search)
        .subscribe(
          res => {
            this.speeches = res;
            EmitterService.get("loader").emit(false);
          },
          err => {
            console.error(err);
            EmitterService.get("loader").emit(false);
          }
        );
    }, 1500);
  }

}
