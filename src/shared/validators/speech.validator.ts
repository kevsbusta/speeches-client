import { Speech } from '../models/speech'

export class SpeechDataValidator {
  validateData(speech: Speech) {
    return speech.title && speech.author && speech.keyword;
  }
}
