export class Speech {
  constructor(){}
  id: number;
  title: string;
  author: string;
  keyword: string;
  content: string;
  date: Date;
}
