import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { HttpClient } from './httpClient.service'
import { Speech } from '../models/speech'

@Injectable()
export class SpeechService {

  // HttpClient is a wrapper of http
  // not being used yet, it is for the backend api
  constructor(private http: HttpClient) {}

  getSpeeches(search: string = null) : Observable<Speech[]> {
    let speeches = this.getSpeehes();
    speeches = search ? speeches.filter(x => x.title.includes(search) || x.content.includes(search) || x.keyword.includes(search)) : speeches;
    return this.http.get("")
      .map((res:Response) => speeches)
      .catch(this.handleError);
  }

  getSpeech(id: number) {
    let speeches = this.getSpeehes();
    let speech = speeches.filter(x => x.id == id)[0];
    return this.http.get("")
      .map((res:Response) => speech)
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error());
  }

  // Mock data
  private getSpeehes() {
    return [
      {
        "id": 1,
        "title": "I Have a Dream",
        "author": "Martin Luther King",
        "keyword": "tech, programming, computer",
        "content": "I have a dream that one day down in Alabama, with its vicious racists, with its governor having his lips dripping with the words of interposition and nullification – one day right there in Alabama little black boys and black girls will be able to join hands with little white boys and white girls as sisters and brothers. \n\n I have a dream today. \n\n I have a dream that one day every valley shall be exalted and every hill and mountain shall be made low, the rough places will be made plain, and the crooked places will be made straight, and the glory of the Lord shall be revealed and all flesh shall see it together.",
        "date": "1963-04-23T18:25:43.511Z"
      },
      {
        "id": 2,
        "title": "Radio Address",
        "author": "King George",
        "keyword": "tech, programming, computer",
        "content": "In this grave hour, perhaps the most fateful in history, I send to every household of my peoples, both at home and overseas, this message, spoken with the same depth of feeling for each one of you as if I were able to cross your threshold and speak to you myself. \n\n For the second time in the lives of most of us, we are at war. \n\n Over and over again, we have tried to find a peaceful way out of the differences between ourselves and those who are now our enemies, but it has been in vain.",
        "date": "1939-04-23T18:25:43.511Z"
      },
      {
        "id": 3,
        "title": "Speech 3",
        "author": "Kevin Bustamante",
        "keyword": "tech, programming, computer",
        "content": "Content 3",
        "date": "2012-04-23T18:25:43.511Z"
      },
      {
        "id": 4,
        "title": "Speech 4",
        "author": "Kevin Bustamante",
        "keyword": "tech, programming, computer",
        "content": "Angular 2 Angular 2 Angular 2 Angular 2 Angular 2 Angular 2 Angular 2 Angular 2",
        "date": "2012-04-23T18:25:43.511Z"
      },
      {
        "id": 5,
        "title": "Speech 5",
        "author": "Kevin Bustamante",
        "keyword": "tech, programming, computer",
        "content": "Angular 2 Angular 2 Angular 2 Angular 2 Angular 2 Angular 2 Angular 2 Angular 2",
        "date": "2012-04-23T18:25:43.511Z"
      }
    ];
  }
}
