import {EventEmitter} from '@angular/core';

export class EmitterService {
  private static _emitters: { [channel: string]: EventEmitter<boolean> } = {};
  private static _not_emitters: { [channel: string]: EventEmitter<string> } = {};
  static get(channel: string): EventEmitter<boolean> {
    if (!this._emitters[channel])
      this._emitters[channel] = new EventEmitter();
    return this._emitters[channel];
  }
  static notify(channel: string): EventEmitter<string> {
    if (!this._not_emitters[channel])
      this._not_emitters[channel] = new EventEmitter();
    return this._not_emitters[channel];
  }
}
