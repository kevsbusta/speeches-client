import { Injectable} from '@angular/core';
import { Http, Headers } from '@angular/http';

// This class is for the backend api.
@Injectable()
export class HttpClient {

  private baseUrl = "http://localhost:4200/";
  constructor(private http: Http) {}

  createAuthorizationHeader(headers: Headers) {
    // headers.append('Authorization', 'Basic ' +
    //   btoa('username:password'));
  }

  get(url) {
    let headers = new Headers();
    // this.createAuthorizationHeader(headers);
    return this.http.get(url, {
      headers: headers
    });
  }

  post(url, data) {
    let headers = new Headers();
    // this.createAuthorizationHeader(headers);
    return this.http.post(url, data, {
      headers: headers
    });
  }
}
