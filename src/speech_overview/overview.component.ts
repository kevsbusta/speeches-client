import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Speech } from '../shared/models/speech';
import { SpeechService } from '../shared/services/speech.service';
import { ActivatedRoute } from '@angular/router';
import { EmitterService } from '../shared/services/emitter.service';

@Component({
  selector: 'app-root',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class SpeechOverviewComponent {
  speech: Speech;
  id: number;

  constructor(private route: ActivatedRoute, private speechService: SpeechService, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
    });

    this.speechService.getSpeech(this.id)
      .subscribe(
        res => {
          this.speech = res;
        },
        err => {
          console.error(err);
        }
      );
  }

  saveSpeech() {
    EmitterService.get("loader").emit(true);
    setTimeout(() => {
      EmitterService.get("loader").emit(false);
      EmitterService.notify("notification").emit('Speech has been updated.');
    }, 1000)
  }

  deleteSpeech() {
    EmitterService.get("loader").emit(true);
    setTimeout(() => {
      EmitterService.get("loader").emit(false);
      this.router.navigate(['speeches']);
      EmitterService.notify("notification").emit('Speech has been deleted.');
    }, 1000)
  }

  shareSpeech() {
    EmitterService.get("loader").emit(true);
    setTimeout(() => {
      EmitterService.get("loader").emit(false);
      EmitterService.notify("notification").emit('Speech has been shared to a team member.');
    }, 500)
  }
}
